import RPi.GPIO as GPIO
import time, os

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
red = 11
green = 13
blue = 15
y=0

GPIO.setup(red, GPIO.OUT)
GPIO.setup(green, GPIO.OUT)
GPIO.setup(blue, GPIO.OUT)

for i in range(10):
	GPIO.output(red, 0)
	GPIO.output(green, 1)
	GPIO.output(blue, 1)
	time.sleep(0.5)
	GPIO.output(red, 1)
	GPIO.output(green, 1)
	GPIO.output(blue, 1)
	time.sleep(0.5)

GPIO.output(red, 0)
GPIO.output(green, 0)
GPIO.output(blue, 0)
time.sleep(3)

while True:
	try:
		with os.popen('vcgencmd measure_temp') as temp:
			for line in temp:
				pass
			x=float(line.split('temp=')[1].split()[0].split('\'C')[0])

		if x!=y:
			if(x <= 35):
				GPIO.output(blue, 0)
				GPIO.output(green, 1)
				GPIO.output(red, 1)
			if(x > 35 and x <= 60):
				GPIO.output(green, 0)
				GPIO.output(blue, 1)
				GPIO.output(red, 1)
			if(x >= 60):
				GPIO.output(red, 0)
				GPIO.output(blue, 1)
				GPIO.output(green, 1)
			
			y=x
		time.sleep(1)

	finally:
		GPIO.cleanup()
	
GPIO.cleanup()
